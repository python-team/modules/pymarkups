pymarkups (4.1.0-1) unstable; urgency=medium

  * New upstream release.
  * Update debian/watch:
    - Support tarball file names starting with a lowercase letter.
    - Stop checking PGP signature, PyPI no longer supports that.
    - Bump version to 4.
  * Add python3-mdx-math to Recommends.
  * Bump copyright year.
  * Bump Standards-Version to 4.7.0, no changes needed.
  * Add python3-pymdownx to autopkgtest dependencies.

 -- Dmitry Shachnev <mitya57@debian.org>  Mon, 09 Dec 2024 15:50:21 +0300

pymarkups (4.0.0-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on dh-python, python3-all and
      python3-markdown.
    + python3-markups: Drop versioned constraint on python3-markdown in
      Recommends.
    + python3-markups: Drop versioned constraint on retext in Breaks.

  [ Dmitry Shachnev ]
  * New upstream release.
  * Drop pygments_2.11.patch, included in the new release.
  * Build with pybuild-plugin-pyproject.
  * Update debian/copyright.
  * Bump python3-setuptools build-dependency to 61.2.
  * Add Markups.egg-info/ to debian/clean.

 -- Dmitry Shachnev <mitya57@debian.org>  Tue, 17 Jan 2023 14:35:05 +0400

pymarkups (3.1.3-2) unstable; urgency=medium

  * Add a patch to fix build with Pygments 2.11 (closes: #1006041).

 -- Dmitry Shachnev <mitya57@debian.org>  Sat, 19 Feb 2022 19:39:05 +0300

pymarkups (3.1.3-1) unstable; urgency=medium

  * New upstream release.
  * Remove skip_doctest_docutils_0.17.patch, no longer needed.
    - Bump docutils build- and test dependency to 0.17.

 -- Dmitry Shachnev <mitya57@debian.org>  Mon, 22 Nov 2021 18:06:46 +0300

pymarkups (3.1.2-1) unstable; urgency=medium

  * New upstream release.
  * Drop update_doctest.patch, applied upstream.
  * Add a new patch to skip part of doctest that needs docutils 0.17.
    Currently 0.17 is available only in experimental, unstable has 0.16.
  * Bump Standards-Version to 4.6.0, no changes needed.
  * Declare Rules-Requires-Root: no.

 -- Dmitry Shachnev <mitya57@debian.org>  Tue, 07 Sep 2021 18:14:36 +0300

pymarkups (3.1.1-1) unstable; urgency=medium

  * New upstream release.
  * Add a patch to make the doctest pass with recent changes.

 -- Dmitry Shachnev <mitya57@debian.org>  Fri, 05 Mar 2021 19:59:50 +0300

pymarkups (3.1.0-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Ondřej Nový ]
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address

  [ Dmitry Shachnev ]
  * New upstream release.
  * Update my key in debian/upstream/signing-key.asc.
  * Add python3-yaml to Build-Depends, Recommends and test depends.
  * Remove no longer needed dependency on python3-pkg-resources.
  * Add python3-pygments to Build-Depends.
  * Update to debhelper compat level 13.
  * Bump Standards-Version to 4.5.1, no changes needed.

 -- Dmitry Shachnev <mitya57@debian.org>  Sun, 31 Jan 2021 22:02:14 +0300

pymarkups (3.0.0-2) unstable; urgency=medium

  [ Dmitry Shachnev ]
  * Make the autopkgtest depend on python3-pygments.
  * Make the autopkgtest depend on python3-all, and use py3versions -s
    instead of -i.
  * Update to debhelper compat level 12.
  * Bump Standards-Version to 4.5.0, no changes needed.

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

 -- Dmitry Shachnev <mitya57@debian.org>  Fri, 20 Mar 2020 14:57:39 +0300

pymarkups (3.0.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/watch: Use https protocol
  * d/tests: Use AUTOPKGTEST_TMP instead of ADTTMP
  * d/control: Remove ancient X-Python3-Version field

  [ Dmitry Shachnev ]
  * New upstream release.
  * Add build-dependencies on python3-mdx-math and python3-setuptools.
  * Bump Breaks on retext to (<< 6.0~).
  * Update copyright years in debian/copyright.
  * Update debhelper compat level to 11.
  * Add runtime dependency on python3-pkg-resources.
  * Add Markups.egg-info/ to pybuild.testfiles to make the tests pass.
  * Bump Standards-Version to 4.1.5, no changes needed.

 -- Dmitry Shachnev <mitya57@debian.org>  Fri, 06 Jul 2018 15:26:04 +0300

pymarkups (2.0.1-1) unstable; urgency=medium

  * New upstream bugfix release.
  * Bump Standards-Version to 4.0.0, no changes needed.

 -- Dmitry Shachnev <mitya57@debian.org>  Thu, 29 Jun 2017 16:06:03 +0300

pymarkups (2.0.0-1) unstable; urgency=medium

  [ Dmitry Shachnev ]
  * Add Breaks on old versions of ReText (less than 5.1).
  * New upstream release.
  * Bump Standards-Version to 3.9.8, no changes needed.
  * Bump year in debian/copyright.

  [ Ondřej Nový ]
  * Fixed VCS URL (https)

 -- Dmitry Shachnev <mitya57@debian.org>  Sat, 14 May 2016 19:17:47 +0300

pymarkups (1.0.1-1) unstable; urgency=medium

  * New upstream release.
  * Add python3-textile to build-dependencies, test dependencies, and
    the Recommends: list for python3-markups, now that it's available.
  * Remove obsolete debian/python3-markups.examples file.

 -- Dmitry Shachnev <mitya57@debian.org>  Tue, 22 Dec 2015 17:43:19 +0300

pymarkups (1.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Update Homepage URL.
  * Update package description.
  * Drop custom dh_auto_test override, let pybuild do the right thing.
  * Bump X-Python3-Version to 3.2, following upstream.
  * Add an autopkgtest.
  * Update Vcs fields for Git migration.

 -- Dmitry Shachnev <mitya57@debian.org>  Sun, 13 Dec 2015 22:12:45 +0300

pymarkups (0.6.3-1) unstable; urgency=medium

  * New upstream bugfix release.
  * Require python3-markdown 2.6, the mathjax extension does not work
    with older versions.

 -- Dmitry Shachnev <mitya57@debian.org>  Sat, 20 Jun 2015 20:10:57 +0300

pymarkups (0.6.1-1) unstable; urgency=medium

  * Upload to unstable.
  * New upstream release.
  * Update debian/watch to use pypi.debian.net redirector.
  * Add my key to debian/upstream/signing-key.asc and make uscan verify
    the signature.

 -- Dmitry Shachnev <mitya57@debian.org>  Sun, 19 Apr 2015 12:17:05 +0300

pymarkups (0.6.0-1) experimental; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 3.9.6, no changes needed.
  * Bump copyright year in debian/copyright.

 -- Dmitry Shachnev <mitya57@debian.org>  Sun, 25 Jan 2015 20:00:24 +0300

pymarkups (0.5.2-1) unstable; urgency=medium

  * New upstream bugfix release.
    - Fixes loading of Markdown extensions with modules (closes: #768179).

 -- Dmitry Shachnev <mitya57@debian.org>  Wed, 05 Nov 2014 21:07:10 +0300

pymarkups (0.5.1-1) unstable; urgency=medium

  * New upstream bugfix release.
  * Explicitly build-depend on dh-python.
  * Update my e-mail address.

 -- Dmitry Shachnev <mitya57@debian.org>  Tue, 16 Sep 2014 10:22:49 +0400

pymarkups (0.5.0-1) unstable; urgency=medium

  * New upstream release.
  * Update URLs for upstream move from Launchpad to GitHub.
  * Convert to pybuild.
  * Bump Standards-Version to 3.9.5, no changes needed.

 -- Dmitry Shachnev <mitya57@gmail.com>  Fri, 25 Jul 2014 22:51:18 +0400

pymarkups (0.4-1) unstable; urgency=low

  * New upstream release.
  * debian/watch: use HTTPS, and correctly escape dots.

 -- Dmitry Shachnev <mitya57@gmail.com>  Sat, 30 Nov 2013 13:27:43 +0400

pymarkups (0.3-1) unstable; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Dmitry Shachnev ]
  * New upstream release.

 -- Dmitry Shachnev <mitya57@gmail.com>  Thu, 25 Jul 2013 14:30:31 +0400

pymarkups (0.2.4-1) unstable; urgency=low

  * Initial release (Closes: #687824).

 -- Dmitry Shachnev <mitya57@gmail.com>  Sun, 25 Nov 2012 18:13:08 +0400
